# Minutes for the community meetup on 2024-06-12

 * Start: 17:00 UTC; End: 18:00 UTC

 * 7 participants [names removed to protect participant privacy]

 * *Where*: BigBlueButton

 * *Topic*: This week Enrique Barba Roque, a master's student at TU Delft with Luis Miranda da Cruz [1] and Thomas Durieux [2], will present his work on energy hotspots and inefficiencies in different Linux distributions and libc implementations. In their experimental work they have found inefficiencies in TCP communication in Alpine vs. Ubuntu and would like to present and discuss their findings with community.

[1] https://www.tudelft.nl/en/staff/l.cruz/
[2] https://durieux.me/

 * See also Jupyter notebook: https://github.com/enriquebarba97/energy-hotspots

## Minutes

### Presentation

 * Introductions
 * Presentation (see PDF: "2024-06-12_presentation_Energy_Inefficiencies_Docker.pdf" for details)
   + Why this work? Cloud computing centers consume a lot of energy and have high CO2 emissions.
   + Goal: To make inefficiencies more transparent with aim to fix them.
   + How: We compared different workloads using different base images to build docker images. Alpine/musl had the highest energy consumptions, despite run times being more or less the same as, e.g., Ubuntu/glibc.
   + Prior hypothesis: Alpine uses musl, whereas all others use standard C library, and this explains the difference; however, this hypothesis was unchecked.
   + This thesis:
     - Compare glibc vs. musl by switching libraries to check.
     - Use tracing to compare shared libraries.
     - Locate the source of inefficiencies.
   + Comparing glibc and musl
     - glibc has been argued to be bloated and slow, whereas musl has been claimed to be clean, efficient, with minimal bloat.
     - Tried to isolate this difference in distro/libc versions using Redis: same Redis version, differences only in libc.
     - To replicate prior thesis, install Redis from source so exactly the same, add glibc to Alpine and run Redis compiled against it.
   + Testing hypothesis from prior study: Higher energy consumption driven by Alpine/musl
     - Result: Alpine/glibc had lower energy consumption and was comparable to Ubuntu
   + Tracing
     - Record calls made by the libraries to find energy hotspot.
     - `uftrace` was used, but it creates a lot of data, so reduced the amount of requests.
     - `memcpy` the most called function (see table on p. 17 in PDF)
     - Analysis of tracing
       + Normalizing by finding checkpoints in the logs to find sections running the same task, locating unique lines.
     - Create histogram of functions, `memcpy` increases a lot, this could explain the difference.
       + Copy small elements 1M times: similar behavior across all three, but when having 1 memory buffer Alpine/musl has a much higher energy usage and takes longer.
       + Significant difference in energy performance for `memcpy` function between musl and glibc
   + Read and write with a TCP server
     - Using `memcpy` in the TCP server to replicate behavior of Redis
     - Here we see Alpine/musl has higher energy consumption. This is likely not the only workload that shows a hit in performance.
     - Performance gap result: musl is not optimized in certain scenarios, whereas glibc has been optimized for many different contexts. Alpine/musl trade performance for smaller code base, whereas glibc trade larger code base for better optimization.
   + This methodology could help locate inefficiencies

### Q(uestions) & A(nswers) & C(omments)

 * Q: Have you looked at the source script to understand what exactly is happening with `memcpy`?
   + A: glibc provides assembly code that is architecture specific, can align assembly code to improve efficiencies; musl uses one function for all calls. This is a tradeoff in order to have a clean codebase.
 * Q: What were you using for the energy measurement?
   + A: A dedicated machine with a workload as isolated as possible. We measured with energy profiler using internal sensors.
 * Q: What register can read a specific core?
   + A: We are using AMD with a profiler designed by Thomas. It reports energy usage of the cores individually.
   + C: AMD provided the energy measurements per core, on Intel this is not possible.
 * Q: Where is the Jupyter notebook?
   + A: The notebook is the analysis: https://github.com/enriquebarba97/energy-hotspots. We used dynamic patching. When you call the tracer you can pass an option to dynamically patch the binary.
 * Q: About the workloads: Can you share some of your thoughts on your thinking about data centers?
   + A: The main reason we chose Redis is it is an easier workload to test. It doesn't have any other dependencies, so we can isolate differences more easily. We looked into `Postgres`, also written in C language, and we observed a difference in energy draw too (this is also in the notebook). But we did not have time to run another benchmark to confirm it.
   + C: Benchmarks: TCP, TCPH (??) are good for comparing benchmarks, they provide a good stress and are reproducible.
