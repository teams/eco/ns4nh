# Minutes for the community meetup on 2024-04-10

* Start: 17:00 UTC; End: 18:00 UTC

* 8 participants [names removed to protect participant privacy]

* *Where*: BigBlueButton

* *Topic*: Planning the Goals Sprint

Minutes of the meetup are at the end of this document.

## To Discuss

 * Goals Sprint: https://community.kde.org/Sprints/Goals/2024
 * Other
   + Setting up VNC in lab
   + LibreOffice measurements
   + Monthly KEcoLab Tooling meetup

## Minutes

 * Sprint: List of ideas at wiki, topics we can work on; goal to bring together the three main goals; looking at ideas at https://community.kde.org/Sprints/Goals/2024
   + KEcoLab: working on documentation and tooling, integration of more apps, dashboards of measurements
     - Usage scenario scripts for Okular may need updates
     - VNC server / Libreoffice needs to be worked on
     - KdeEcoTast integration in KEcoLab
     - BE certification: presenting results so users can understand them, looking to the future
     - Open topic to wider community, collect views from the community
   + NS4NH project
     - Presentation of the project to the kde community Saturday. Remote presentation for KDE community, when to do this? 11 CEST?
     - Brainstorming session locally
     - Design work for the leaflet
     - Inviting others to join, GNOME also interested in windows retirement, would be helpful to have an agenda/schedule
     - Schedule: make a 3-column table for each goal
     - Hybrid session can also be asynchronous (with chat)
   + Usage scenarios, tooling for this
     - KdeEcoTest: Windows support testing and documentation
   + Creating visibility for sustainability, labelling for users
   + AI and sustainability, transparency for users
     - Late afternoon on Saturday, may have wider community interest, including policy suggestions for how things can be incorportated into software, use saturday afternoon session to plan it
     - Start with smaller discussion among ourselves, invite others at a later time (sunday?)
     - Issues brought up in fedora: Is it data, Is it code? How does it relate to Free Software principles? Packaging software brings up technical and practical questions. See: https://lists.fedoraproject.org/archives/list/legal@lists.fedoraproject.org/thread/PIPILJCMDEO67ORL4SAKB3NPHHVMFDJE/
     - See also KDE community discussions, add links to the wiki.
   + Dolphin
 * Remote participation / Hybrid sessions
   - MBition has a setup (screens, cameras, mics), we can test in advance
   - A laptop can already get us pretty far
   - Can we get a handheld microphone? Maybe
 * Todos
   - Make draft of schedule
   - Extend invitation within local community (new-project affiliated contributors, GNOME, etc.)
   - Advertise to non-local community, get a sense of who wants to join and where they are so we can plan remote sessions
   - Test technical setup (microphones)
