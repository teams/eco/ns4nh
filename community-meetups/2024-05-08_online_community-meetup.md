# Minutes for the community meetup on 2024-05-08

* Start: 17:00 UTC; End: 18:00 UTC

* 3 participants [names removed to protect participant privacy]

* *Where*: BigBlueButton

* *Topic*: See pad https://collaborate.kde.org/s/cactBt4frrfTjbW

Minutes of the meetup are at the end of this document.

## To Discuss

 * Article by Markus about KDE Eco for Golem
 * KEcoLab progress
   + R / LaTeX installation (see https://invent.kde.org/teams/eco/remote-eco-lab/-/issues/15 , https://invent.kde.org/teams/eco/remote-eco-lab/-/issues/17)
   + variable paths for scripts (see https://invent.kde.org/teams/eco/remote-eco-lab/-/issues/14)
 * Discussion: Merging feep / KEcoLab repos?
 * Status of packaging Selenium/KdeEcoTest in KDE Neon
 * Updates in project "Sustainable Software For Sustainable Hardware"
 * Scripts for Okular v. Adobe Acrobat
 * Eco tab
   + Appstream implementation: need link by defining a custom tag in the application repository, see <https://invent.kde.org/graphics/okular/-/blob/master/shell/org.kde.okular.appdata.xml>
   + Generation script for apps.kde.org

## Minutes

 * Introductions
 * Publication by Markus about KDE Eco community in e.g. Golem (second larges IT portal)
   + Also, there is a colleague who writes for Linux magazine
 * Possible article ideas
   + Okular
     - focus on tenders to move the market
   + Measurement tools Selenium / KdeEcoTest
   + New project about hardware longevity
     - mention Wondows 11 end of life, etc.
     - references to right to repair and Apple
 * Article content
   + Not too deep on the technical layer
   + What do we want to be written?
   + Who is the target audience?
   + Overview about what happens in the project
 * How can we get articles into other magazines, i.e. Stiftung Warentest, Ökotest?
   + Find out business model to know how to approach them
