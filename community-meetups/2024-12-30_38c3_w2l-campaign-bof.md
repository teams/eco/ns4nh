# BoF: Win2Linux (W2L) Campaign

## General info

**Organizational**

 * Date/Time: 2024-12-30 13:00-15:00 (CET)
 * Participants: ca. 20-25 people
 * Location: Bits und Bäume Habitat, Workshop Area
 * Link to pad: <https://annuel.framapad.org/p/38c3-w2l-abzn?lang=en>

**Planning Timeline**

 * Online meetup last week of January (tbd)
 * FOSDEM BoF 2025-02-01--02

**Contact**

 * W2L Matrix room: <https://matrix.to/#/#win2linux:kde.org>
 * W2L Gitlab repo: <https://invent.kde.org/teams/eco/opt-green/-/milestones/9>
 * Email: joseph@kde.org
 * See also KDE Eco Matrix room: #kde-eco:kde.org

**Info**

> Windows 10 security updates end on 14 October 2025. This happens to be KDE’s 29th birthday and also, ironically, International E-Waste Day from the WEEE Forum. Hundreds of millions of functioning devices will become e-waste. This means manufacturing and transporting new ones, which is perhaps the biggest waste of all: hardware production alone can account for over 75% of a device’s CO2 emissions over its lifespan. Free Software is a solution, and if we work together Windows 10 could truly be the last version of Windows users ever use!
> 
> We are coordinating a global, unified Free Software campaign over the next year to raise awareness about the environmental harm of software-driven hardware obsolescence, at the same time upgrading users from Windows 10 to GNU/Linux directly to keep those devices in use and out of the landfill. Let’s think big and act boldly! What should this campaign look like? Who is the target audience and how can we best reach them? What obstacles for new users exist and how can our communities best address them?

 * SFSCon BoF discussion: <https://invent.kde.org/teams/eco/opt-green/-/issues/66>

## To-Dos from 38C3 BoF (see notes below): 

 * Create first iteration of website with information about the campaign
   - possible URL: endof10.org
 * Awesome list of organization doing similiar things
   - Windows 10 EoL campaigns (e.g., Computertruhe)
   - Companies or organization offering support or installation events for Linux (e.g., CryptoParty)
 * Organize next online meet-up
   - last week of January (before FOSDEM) 

## Notes

**Structure**

 * Intro to campaign idea and overview of past discussions (ca. 10 minutes)
 * General discussion (ca. 20 minutes)
 * Discussions in three smaller groups (ca. 60 minutes)
   - Group 1: target audience / support networks
   - Group 2: arguments for migrating to Linux (white paper), and
   - Group 3: use cases and solutions
 * Overview of group discussions (ca. 20 minutes)
 * To-dos and wrap-up (ca. 10 minutes)

### **Intro & general discussion**

 * observation: under 25 and over 65 most incentivized to change (i.e., not in career phase)
 * from past discussions:
   - young environmental activists (last generation, extinction rebellion, FFF)
   - tech savvy / hackers to help family & friends ("Hackers, Let's Help")
 * should target students/pupil too
   - make it easy for them to their everyday work, writing thesis or making presentation
 * remote desktop software alternatives for team viewer (???)
 * adoption strategy:
   - first introduce FOSS cross-platform application as alternatives (for example, Libreoffice) before migrating to Linux system
   - see Schleswig-Holstein migration to Linux as example, Munich/Munix as counter-example
 * repair cafes, installation events, support centers for helping with Linux system in local communities.
 * create arguments for admins or responsible persons to justify the migration from Windows to Linux to persuade their employer
 * target organizations or companies with not much funding for mirgration to Windows 11
   - schools are a good example
 * define/find toolsets, maybe certain distributions, for certain instustries/sectors
 * procedural proposal: let's break up into groups
   - Group 1: target audience / support networks
   - Group 2: arguments for migrating to Linux (white paper), and 
   - Group 3: use cases and solutions

#### Group 1: Target audience / support networks

**target audience**

 * eco activists may not care about tech
   - Opt Green project idea is to align values of eco-consumers / environmentalists with tech consumptions and use
 * multipliers who get exited and "spread the word"
 * teachers
   - also multipliers
   - example of Hannover workshops (see blog post: <https://engineering.upvest.co/posts/sponsoring-future-engineers/>)
 * let's target potential support networks themselves (hackers, environmentalists, etc.)
   - each group can tailor the message to their communities
   - if we have too many messages it may get unclear
 * university students?
   - for many "End of 10" will not an issue, Uni pays for windows licensing
   - Microsoft has agressive marketing, students get office, onedrive, … for free
 * the "I don't care" population?
   - outside the scope of the campaiign, let's focus on those who are open to change
 * marginalized groups (refugees, women, PoC)
   - refugees might be hard to support because of language barriers
 * target tech-savvy people only? general tech audience?
 * target those who use Win10 and cannot upgrade
   - "End Of 10" slogan
   - endof10.org is still available

**support networks**

 * installing vs training and keep using the software
 * support groups in Berlin: digital zebras, topio
 * focus on tasks rather than people
   - CryptoParties might be a good reference, open for everyone and not for specific groups
 * event ideas:
   - „Bring your devices back to life“
   - "digital eco-party" (like CryptoParty but with focus on enviromental issues)
 * there will be a sweet spot of time when security issues discussed in media
   - who do people contact when they become aware they will have a problem (sec,performance)?
 * volkshochschule
   
**other**

 * what about chromeos (flex)?
   - if it keeps devices out of the landfill and it is FOSS, then it should be part of the campaign
 * To-do
   - Set up website, have regional groups, opening hours, contact info, all easy to read in one page

#### Group 2: Arguments for migrating to Linux (white paper)

**Use cases** \& **target audience** \& **migration strategies**

 * Special Groups like Journalists work with the windows software they know and because most use windows new software is written for windows. We want Linux make the default, so Linux is written for Linux instead 
 * Appstore is useful/necessary for most users
 * To convince people it has to be easier with Linux than Windows 
 * Maybe we can find heroes as example users for each user group 
 * An argument to convince people is that Linux updates, when you want, while windows forces updates on you. 
 * For the average user we want the LTS version. Advanced users may want always up to date users, but the people we convert probably should start with a more stable distro. 
 * Create user-groups and define toolsets / installs / flavors
 * People learn by mimicking, show them and make it easy (easier than Windows)
 * Examples:
   - Journalists: Linux protects sources better (no AI, no surveillance), other tools than veracrypt, work with Nextcloud? 38C3 talk yesterday how2linux for journalists
   - Teachers (MS and apple pay schools to use their products, give devices as present, etc. = hard to compete)
   - Schools (LMS, teaching software) 
   - No-Cloud-Offline 
   - Gamers: steam deck users don't even want windows because Linux works better, old games work best on Linux, Heroic game louncher, universal game launcher, bazzite, Batocera
   - (Media) Content creators: good phone integration, video conference, video recording, image editor, video cutting, podcast recording, moderation of chat
   - Marketing / Sales
   - Accounting / Controller (ERP-System , banking, ) 
   - Private family computer (photo, office, etc. ) 
   - Medical (Doctors) 
   - ERP-system 
   - Developers 
   - Data-Researchers: Social Science, Replace QGis, Excel, Google Forms, PowerBI, hyphe research participation tools, data visualisation 
     + Available: QGis, Libreoffice, QPlot (too complex) 
     + Libreoffice Calc vs Excel -> works differently
     + Missing: Examples
     + Nexcloud Forms with connection to Grafana to automatically create dashboards and tools to automatically create Presentations out of it
 * Hard facts about environmental impact of software
 * Strategy: Negative Campaign about windows
   - No AI
   - No Ads
   - No cloud stuff
 * How to convince people of cloud stuff
   - Windows is made easy
   - Linux: No standard way, nextcloud exists, but provides hoster ( \& usually extra money )

**Pro arguements**

 * Linux is more transparant and configurable for end users too. Can lead to more independencies of the end users.
 * Windows does not make decisions in the interest of their customer.
 * Windows 11 is very different from windows 10. So workshops are needed anyway.
 * WSL is a prove that Linux is usable. 
 * There are solutions providing support and LTE maintance. See red hat as an example.
 * Since it is open source own IT can fix, change and modify software/system for own needs of the companies.
 * Data privacy. Point out to re authority confirming open source/Linux is actually safer and more data privacy respecting
   - BSI published a blog post about the strategic advantages of Open Source: https://www.bsi.bund.de/EN/Themen/Unternehmen-und-Organisationen/Informationen-und-Empfehlungen/Freie-Software/freie-software.html
 * Can be used for conveying the idea that you care about the data privacy by using Linux.
 * Migrating will save us from buying a lot new laptops.
 * There is no license cost for just using Linux
 * No forced AI features
 * No forced updates
 * Less moments to download random executables. There are package managers, application stores. Better for security
 * Linux is quite out there for quite a while. Battle proven.
 * Better textual support/forums.
 
**Concerns of deciders to switch to GNU/Linux**
 * Who is my busniness partner for support, liability issues?
 * Many people have grown up with window systems
 * I will have to do an extensive software training for any person in my company/institiution, where do I get the trainers and person set this up.
 * My boss Level in in gonvernment might reject it due to financial interest of the business interests
 * Hardware compatibility with my existing infrastructure printers eg.
 * We have special applications only running on windows
 * Workflow integration; existing templates links to proprietary cloud services
 * Insurance issues
 * Open source project can be overstressed by the increased demand. Support and new features could not be implemented since project is under a lot of pressure.
 * A person told me 10 years ago its awful only to use it if you're a nerd
 * Less oppotunity to get professional support over hotline or video calls
 * Windows is preinstalled and not Linux
 * There is the misconception that open softeare is less secure. 

**ideas regarding the concerns**

 * Begin with reflection: 
   - What language reminds people of old bad memories when first trying Linux?
   - What are the specific concerns of the easy first target audience?
   - Who are our allies in strategy? How can we help? How can we keep trust and motivation?
   - Getting feedback from success stories (Gendbuntu)
   - Gettng feedback from failed stories
 * Starting with a plan, step by step, begining with the easy target audience (teacher, youth company, sensitive sector..)
 * In France there are financial interest lock at the hight level : what we do ?
 * Starting slow communication to prepare the minds slowly, to make linux a known entity (Ex: talk at strategy event according to easy target as <https://numerique-en-communs.fr/>
 * Who can pay for a 3 years migration plan ? (Animation, tool production, investigation, communication...) Maybe the people who are going to earn a living from this paradigm change: who are there ? How much they can give ? What are the mobilisable resources we can get ?
 * printer company interlocutor to get linux compatility garantee

#### Group 3: use cases and solutions

This group worked offline. See the following photos for documentation of their notes:

 * 2024-12-30_38c3_w2l-campaign-bof_group-3_1.jpg
 * 2024-12-30_38c3_w2l-campaign-bof_group-3_2.jpg

