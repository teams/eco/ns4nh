# Minutes for the Opt Green BoF at Akademy 2024 on 2024-09-09

 * Start: 10:00 CEST; End: 10:50 CEST
 * About 20 participants
 * Note: These minutes were compiled both in person (special thanks to the volunteer!) as well as days after the BoF, so some details may be missing. Please feel free to add information to these minutes if you were in attendance.

## Opt Green

 + How can we make energy consumption visible?
   - Calculator - how much CO2 to I save - check if this exists
   - make savings visible with energy price comparison
   - make comparisions for basic need programms (Mail, Browser, Office suit, Okular)
   - Use visualisations (sounds) to compare power useage
 + kde.org/for KDE Eco is for ...
 + Windows 10 Adds Ideas -> Bart
 + Aligning KDE ECO network in India -> Bhushan
 * See also Issue 'Start to use additionally the "Carbon Footprint" term in the promo materials': https://invent.kde.org/teams/eco/opt-green/-/issues/43

## How low can we go

 * Low System Requirement, see comment: https://invent.kde.org/teams/eco/opt-green/-/issues/40#note_1031231
 * See also "Use cases for older, less powerful hardware": https://invent.kde.org/teams/eco/opt-green/-/issues/36

## Competition which distro on Hardware

 * See issue "friendly 'competition' for the community: find the lowes spec HW KDE Plasma is usable on": https://invent.kde.org/teams/eco/opt-green/-/issues/40

## Translation

 + Workflow, see Issue "Opt Green: translation workflow for leaflet": https://invent.kde.org/teams/eco/opt-green/-/issues/41
 + Translation Danish -> Claus
 + Translation French -> Emmanuel, see Issue "Translate leaflet into French": https://invent.kde.org/teams/eco/opt-green/-/issues/35
 * Related Issue "Port leaflet with InDesign format to a FOSS format": https://invent.kde.org/teams/eco/opt-green/-/issues/38

## Workshops in Hometowns

 + Thomas, see issue "Opt Green: installation workshop planning" https://invent.kde.org/teams/eco/opt-green/-/issues/39
