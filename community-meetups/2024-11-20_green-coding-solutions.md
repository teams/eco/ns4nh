# Minutes for the community meetup on 2024-11-20

* Start: 18:00 UTC; End: 19:00 UTC

* 9 participants [names removed to protect participant privacy]

*When*: Wed. 20 November 18-19h UTC

*Where*: https://meet.kde.org/b/jos-l59-2i1-9yt

*Topic*: Green Coding Solutions tools "Eco-CI" (CI/CD pipeline) and "Powerletrics" (Linux kernel extension)

* *Pad*: Ideas are collected at this pad, please add ideas of your own:

      https://collaborate.kde.org/s/cactBt4frrfTjbW

* *Details*: Eco-CI [0] is an open source GitHub / GitLab Plugin that estimates the energy and carbon emissions of a CI/CD workload. It hooks into the pipeline and will print a summary directly in the logs or as a downloadable artifact. Arne, one of the core developers, will present the tool and show some numbers from repos, including their average emissions, to get a feel for the importance of the topic. A discussion of integrating Eco-CI into Okular's GitLab repository will follow the presentation.

Powerletrics [1] is a kernel extension that brings per process power usage estimations to Linux. It is modelled after the MacOS tool powermetrics which developers can use to gain insights into their environmental impact of their code. With modern ICT infrastructure using more and more resources, it is important that there are easy tools for monitoring and optimisation available so that we don’t waste precious resources.

[0] https://github.com/green-coding-solutions/eco-ci-energy-estimation
[1] https://github.com/green-kernel/powerletrics

## Minutes

 * Introductions
 * Powerletrics presentation
   - See slides
   - Open questions:
     + How important do you find the actual absolute power value rather than a more comparable Energy Impact value. Energy Impact is more representative of the actual software workload happened, and less coupled to the hardware.
     + How do you feel about integrating such a thing in a KDE display at some point. Because I know there were some hestiation to use RAPL before. Was it because of a only root readable endpoint?
 * Eco-CI presentation
   - See slides
 * Q(uestions) & A(nswers) & C(omments)
   - Q: What can we do with the information from Eco-CI? We need the pipeline.
     + A: First step is we need to know how much carbon our pipelines emit. With the data, we can make decisions. Without knowing this value, we cannot do anything. Transparency first and then action.
   - Q: How does development work in KDE?
     + A: We run our own Gitlab instance, and that means we can make decisions at a deeper level. What we could optimize is finding unnecessary pipeline runs, not just individual pipelines.
     + C: The tool works on an individual pipeline level. To get an overview of all pipeline would need to integrate the tool into all pipelines.
   - Q: How do other projects use the Eco-CI tool?
     + C: Developers get the info, but perhaps they don't know what to do with it.
     + C: For developers looking to bring emissions down, what values do they need to make a decision?
     + C: Info can also be used for reporting and transparency.
   - Q: How could Okular use this info?
     + A: On idea: When we compile, we complile on Linux and Windows. If we realize compiling on Windows is 10x more expensive, we can reduce the number of times we run that build, as a first step.
   - Q: How important do you find the actual absolute power value rather than a more comparable Energy Impact value?
     + A: Absolute power values are more easily translatable across contexts, but if the Energy Impact value is more informative, then that makes sense to use.
   - Q: Was the hestiation to use RAPL before because of a only root readable endpoint?
     + A: In short, yes, though it seems powerletrics has found a way around this problem.
