# End Of 10 Sprint

## Organizational

### Background

Windows 10 security updates end on 14 October 2025. This happens to be KDE’s 29th birthday and also, ironically, the WEEE Forum's International E-Waste Day. Hundreds of millions of functioning devices will become e-waste. KDE Eco is spearheading a cross-community Free Software campaign to raise awareness about the role software plays in driving e-waste and promoting GNU/Linux as a solution today. A [mock up of the website](https://invent.kde.org/-/project/17702/uploads/434c5202195ba83b6a264573025ba3a4/end_of_10.pdf) can be found at the Opt Green [milestone for the campaign](https://invent.kde.org/teams/eco/opt-green/-/milestones/9).

### Goals

Upcycling Windows devices to GNU/Linux is an important part of the campaign. In this sprint we will explore various Linux distros (KDE and non-KDE variants) from the perspective of a Windows user installing and using Linux for the first time. The goals of the sprint are two-fold:

 * (1) determine which distro(s) we think are most suitable at the time of the sprint for new users and devices,
 * (ii) outline possible actions for KDE and KDE partners/allies to take in order to best meet the needs and wants of new users.

### Communication channels

 * Sprint chat room: https://matrix.to/#/!dsSkYXwfHrNYrJvOEa:kde.org?via=kde.org&via=matrix.org
 * Invent Milestone: https://invent.kde.org/teams/eco/opt-green/-/milestones/9
 * Campaign chat room: #win2linux:kde.org

### Schedule 

All times CET (UTC+1).

#### Day 1 -- Friday 21 February 09:00-18:00

 * 09:00-09:30 -- Welcome and presentation (Why this sprint? Our goals?) [**hybrid**]
 * 09:30-10:30 -- Discussion and planning [**hybrid**]
 * *10:30-10:45 Break*
 * 10:45-12:15 -- Brainstorm: What do new users expect/need? What does new user success look like? [**hybrid**]
 * *12:15-13:15 Lunch*
 * 13:15-14:45 -- Workshop 1: Installation testing (in groups) [**in person**]
 * *14:45-15:00 Break*
 * 15:00-16:30 -- Workshop 2: Usage testing (in groups) [**in person**]
 * *16:30-17:00 Break*
 * 17:00-18:00 -- Evaluation / documentation from Day 1 workshop sessions [**in person**]
 * *18:30 dinner*

#### Day 2 -- Saturday 22 February 10:00-18:00

 * 10:00-11:00 -- Brainstorm 1: How can KDE benefit from what we learned in Day 1? [**hybrid**]
 + *11:00-11:15 Break*
 + 11:15-12:15 -- Brainstorm 2: How can KDE partners and allies benefit from what we learned? [**hybrid**]
 * *12:15-13:15 Lunch*
 * 13:15-15:15 -- Workshop 1: Installation "market testing" with 6-8 new users* [**in person**]
 * *15:15-15:30 Break*
 * 15:30-16:30 -- Workshop 2: Usage "market testing" with 6-8 new users* [**in person**]
 * *16:30-17:00 Break*
 * 17:00-18:00 -- Evaluation / documentation from Day 2 workshop sessions [**in person**]

*For the afternoon workshops on Day 2, 6-8 participants will join who have limited or no experience with GNU/Linux. The goal is to observe the user journey when installing and using the distros under discussion for the first time.

## Minutes 2025-02-21

### Morning Sessions

 * Participants: 7 in person, 5 online

 * How do we improve the transition from interest in linux to using linux as easy/tolerable as possible. 
   - How do we lower the barriers to access?
   - How do we keep people using the software/OS?
 * Some things we can solve in software. Some things we can solve in messaging/information. How do we rhetoric this?
 * What resources can we provide to new users to give them confidence and keep them engaged?
   - Repair cafes/groups of people who are interested in providing this
   - Manage expectations in the install process
 * Win2Linux - app for windows to help you through the install choosing/setup process, based on your hardware
   - Dev side: how do we make sure that we're working on something we can fix?
   - want to promote KDE stuff most but also note that other options exist? How do we get to the point where KDE is at the top objectively, not because it's skewed or only accounting for KDE stuff. 
 * A lot of problems are distro-related, which is why we're testing stuff today
 * What are the issues for new users?
   - What can we learn from successful newbie distros and bring it into KDE?
 * Goal: reach VLC levels of fame
 * How can people migrate from one system to the other easily?
   - Welcome application exists so you can push buttons and do terminal commands by button instead of using the terminal - 
   - Direct people to the right places for support in the win2linux app
 * Brainstorming new user success issues. Categories:
   - Dealbreakers
     + updates break the system in some way (unreliable updates)
     + hardware breaks/software makes it stop working (webcam, trackpad)
     + Things not working as expected (drivers, peripherals)
     + Too many distro options w/o info
     + No comparable apps/irreplacable apps
     + Missing video codexes
     + Professional apps w/o browser based alternatives
     + CLI
     + Crashes
     + Not a smooth path from new installation to using computer (don't force people into the terminal UI)
     + Can't find the apps in discover/ apps incompatable with OS
     + Data loss (through installation,through forgetting your encryption pw, accidental deleting)
     + levels of encryption are confusing and vary by distro
     + Boot failiure
     + Unreliable updates (not an install issue, but  will be an issue later)
   - Our Unique Selling Points (KDE & Linux universe)
     + Choice of how updates work
     + Ownership (you are in control of your stuff, your device is yours)
     + Customization (you can change everything you want - partition it a little better. *Simple by default, powerful when needed*)
     + no ads (usually)
     + telemetry disclaimer
     + you are not the product 
     + You own your data/no selling your data/ no LLMs/AI/no spying/ Digital Sovereignty/ independence
     + Save money (keep using your hardware, no paying for open office)
     + Community 
     + Interoperability 
     + Choice (even if it's sometimes a lot)
     + Integrated environment
     + Better performance
   - What Users Need
     + Apps to function as expected (need a good email client)
     + Browsers
     + Stability & reliability
     + Essential software out of the box (PDF reader, wordprocessor, etc): need to be able to install super easily. 
     + Support & knowledge of where to find it (paid support, friends, repair cafes, etc)
     + Games? (steamdeck is helping with this)
     + Compatability/interoperability (we support interoperable formats, bring your data with you)
     + Userfriendly setup/installer
   - What Users Want
     + integrated online accounts (all things fit together nicely, easy to setup and sync integrations)
     + things to "just work" (the way they're used to: mitigate expectations)
     + Streaming videos (netflix/jellyfin/hulu/etc)
     + Things they used to do on their previous system (like solitaire)
     + Hardware needs to work the same way/ still being supported
     + "Sex and cookies" (this shouldn't be a difficult switch, it should be something new and slightly different but good. This transition should be like going from car brand A to car brand B, not car to motorcycle or car to truck)
     + Consistent terminology 
     + Battery time (should be the same or improved as current system - do we have data on this? not yet)
     + synchronization between devices? (like onedrive -> syncthing)
     + extensive app store
     + things being pretty
   - What users need but don't know they need
     + media codecs
     + backups
     + firewall
     + driver support
     + friends who use linux
     + knowledge
     + consistent GUI

## Minutes 2025-02-22

### Morning Sessions

 * Participants: 7 in person, 5 online

 * How can KDE benefit from what we learned?
   - what are the best end-user distros?
     + communities should be taken into account for long-term use
     + first experience should be easy
     + make install 3rd party stuff big and easy to see in Fedora
     + Disqualify: Kubunutu becase of lack of healthy long term community
     + Recommend: KDE Fedora with help installing but not solo install, Linux mint for DIY, KDE Debian,
 * New user dealbreakers: when updates break things
   - should make a way to use snapshots/etc so people can easily roll back a change if an update breaks (this is coming down the line, but the tech for doing this doesn't fully exist yet)
 * What can be done to improve:
   - make flatpacks more of a thing so there's more stability so we can solve this problem ^^ 
   - need to explain to the community why this is important and how we stand to benefit: People need a stable system that doesn't break on them
   - make filing bug reports on different components easier (and collab with devs)
 * How do we make bug reporting better/easier
   - to empower repair cafes to use the distros they're comfortable with and we can provide support and info
 * What is the bulk of KDE's work here?
   - outreach? education? support? install parties?
     + currently at awareness raising. Education/outreach to repair cafes rn and convincing them that software is just as important as hardware. 
     + Eventually have resources to send repair cafes and collaborate with the linux community at large. 
     + Having resources prepared and knowing what we can improve and having good messaging. Plasma makes the transition easily. Why do we recommend XYZ distro. 
 * What are the criteria that we think are relevant (rated 1-10)
   - are you installing yourself? do you have in-person support?
   - is the support network healthy?
     + availability of (paid) support?
     + how good is the documentation?
   - ease of installation
   - multimedia codecs
   - stable system / easy rollback option
   - interest in newest software/features vs. sameness
   - familiarity/easy tranistion of UI/UX from your current system
   - specific applications that need to work (work, games)
   - ability to install proprietary software
   - tinker-willing vs. just make it work
   - drivers and hardware support
 * -> what is the state of things with proprietary software on the various distros, perhaps just provide documentation about how to install
 * how can our partners and allies benefit from what we learned?
   - things they can fix that will get new users interested and using their distro
 * what do we want to learn from the experience?
   - why do they want to dual boot?
   - why don't we want to support dual boot (it's just harder)
 * Question for chooser: do you need/want to dual boot?
 * Tickets to make:
   - help center online help section needs to be updated (or techbase needs to be updated)
   - add find support info to KDE/plasma welcome thing
   - Fedora needs to fix their onboarding/install and if they want to be highly recommended they need to adjust (can we get this in the next thing of fedora)
    
### Workshop Session Notes

 * Participants: 13 in person (7 market testing participants, 6 helpers)

 * Choice overload is a problem
 * Getting into the BIOS
 * Choosing USB 
 * keyboard misconfig to US only on BIOS (shift + accent for german)
 * discover should autofill/autocomplete/auto suggest
 * Kubuntu loses language setting on
 * Kubunto partition screen should have a percentage 
   - indicate which partition part is kubuntu and which is not
   - otherwise super easy to set up
 * Wifi card problem (may not be just Fedora)
 * Fedora: let you see your password when you type it
   - dualbooting bootkey problems
   - keyboard config set default to US and should not be
   - NEED TO HAVE EYEBALL TO LET YOU CHECK YOUR PASSWORD ON SETUP TOO
     + and/or type password twice to confirm on setup
 * for some terms on setup it would be helpful to know what the words mean (tooltip or something)
   -  maybe a glossary for these events
 * Videos or channels for learning more 
 * IMPORTANT: TRY WIFI ON LIVE BEFORE INSTALLING 
 * On one laptop Fedora KDE was sluggish and XFCE faster
 * Tour that shows where buttons/files/discover are
 * How to create a USB stick would be a cool thing to teach people
 * Spaces: Berlin Libraries would probably love this
 * TAKE THE USB STICK OUT notification or use a different wallpaper on live vs installed
 * Debrief Notes:
   - top things that come to mind that are positive
     + the UI/X is very familiar to win10 which built confidence 
     + in Kubuntu, everything you need is already there (wordprocessor, browser, codecs)
     + discover is very easy to use
     + the live environment lets you get a real good idea of how things work before you install it
     + feels doable! 
     + fear of trying linux is gone
     + installing linux feels like getting agency back
  - top negatives (none really dealbreakers)
     + a little hard to get into BIOS
     + in fedora, you can't check your password for typos
     + language translation inconsistent
     + need support with installation (fedora)
     + video instructions would be nice
     + handing it to another person or even doing it with people makes it a lot easier
     + confusion about root users/etc
     + Fedora needs to tell you to restart and remove the USB stick
     + downloading things outside the store is difficult (message around the app store, don't visualize it as a store or call it a store to help with that association)
     + a little hard to troubleshoot if you don't know where to start (have to search using your distro as a word)
     + for Win11 new stuff you can do it by commands (debian)
     + can't disable bitlocker
     + a few more prompts would be helpful before going into a surprising black screen (mint and fedora)
     + no way to know which disck you're installing to off the bat
     + compatability with wifi chipset
   - everyone would come to another event, and recommend people try linux, not everyone has a working machine. 
   - Things for new users to test on live:
     + WiFi
     + video streaming
     + browser
     + games/steam
     + apps/signal/etc
     + spotify 
     + editing PDFs
 * KDE Debrief:
   - worth doing this
   - we won't be able to be 100% successful because manufacturer problems we can't solve
