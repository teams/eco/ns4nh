# Minutes for the KEcoLab BoF at Akademy 2024 on 2024-09-09

 * Start: 15:00 CEST; End: 15:50 CEST
 * About 15 participants
 * Note: These minutes were compiled days after the BoF and some details may be missing. Please feel free to add information if you were in attendance.

## KEcoLab

 * Presentation of measurement configuration
   - yaml file and pipeline process
   - R and Rmd scripts
   - bash scripts for emulating user behavior
 * Demo of KEcoLab measurement process
   - opened a VNC connection to monitor bash scripts
   - made a MR for a simplified Okular script
   - monitored pipeline and VNC while scripts were run
     + presented installation process for LaTeX and R environments
   - after pipeline finished, went over the output report
 * Some discussion points
   - Can the measurement be simplified (and thus consume less energy)? See Issue "Choose simplier measurements of the energy consumption to use in pipelines and local machines": https://invent.kde.org/teams/eco/remote-eco-lab/-/issues/23
   - Can we measure KDE Plasma DE itself? Perhaps with different power saving settings?
   - What does the lab setup look like? Which HW do we have?
   - Ideally we would have different HW to measure on, including newer hardware with GPU utilization

## Eco-CI MR from Green Coding Solutions

 * See MR: https://invent.kde.org/graphics/okular/-/merge_requests/1030
 * Discussion points
   - We do not have direct control over energy sources used by data centers: see https://invent.kde.org/graphics/okular/-/merge_requests/1030#note_1031274
   - This kind of information may discourage use of the of CI pipeline

## KdeEcoTest

 * Persistent issue:
   - The KdeEcoTest runner is shifted from a few pixel compared to the position KdeEcoTest records. This is due to the decoration at the top of the window, but not sure, I did not take time to investigate.
   - If memory serves, the problem is on both Windows and on GNU/Linux.
   - Once we will have solved this point, we can compare power measurements for Acrobat reader vs. Okular on Windows.

