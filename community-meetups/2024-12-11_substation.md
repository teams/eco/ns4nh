# Minutes for the community meetup on 2024-12-11

* Start: 18:00 UTC; End: 19:00 UTC

* 3 participants [names removed to protect participant privacy]

*When*: Wed. 11 December 18-19h UTC

*Where*: https://meet.kde.org/b/jos-l59-2i1-9yt

*Topic*: Substation, a collection of developer libraries for carbon-aware computing

* *Pad*: Ideas are collected at this pad, please add ideas of your own:

      https://collaborate.kde.org/s/cactBt4frrfTjbW

* *Details*: As the number of personal computing devices continues to grow, so does the consumption of electrical power. This underscores the urgent need for smarter strategies to harness the green characteristics of our power grids. While many countries are advocating for an increased share of electricity production from green fuel sources, the availability of these sources can be inconsistent. Effectively using these cleaner fuel sources requires real-time awareness of the current and anticipated carbon emissions of the power grid. We present Substation, a comprehensive collection of developer libraries that empower application developers to effortlessly integrate carbon- aware computing into their projects. Substation is designed to optimise application performance based on the carbon intensity of the power grid, enabling applications to reduce their carbon footprint by dynamically adjusting their operations in response to the availability of low-carbon energy.

## Minutes

 * Introductions
 * Background
   - Academic literature overview
   - Industry mostly focusing on reducing electricity use
   - carbon-aware solutions focus on infrastructure, but not consumer oriented
   - Energy consumption / measurement, but only one dimension of the issue
     + also need to consider power grid mix
     + 1 GW of solar is not comparable to 1 GW of coal
 * Carbon-aware computing
   - mostly focused on the cloud
     + location and time of use really vary the carbon impact of these devices
     + algorithmic geographic shifting of workloads based on energy grid characteristics
     + Google has introduced temporal dimension to their workloads: delay or run at lower intensity based on power grid characteristics. This is not geographical but time-based shifting.
   - local carbon-aware
     + course-grain OS level approaches
     + not in a company setting with very well defined processes
     + EcoFreq is one approach that scales CPU/GPU frequencies up or down depending on power grid information. 15% carbon reduction for 10% performance degredation
     + missing is a way to tailor the software for carbon-aware computing
   - state of things
     + Green Software Foundation tool for measuring carbon-intensity of local grid, but only measuring, want to minimize carbon
     + most solutions don't target consumer-tier devices
 * Substation
   - Framework for real-time adjustments based on the carbon intensity of the grid
   - limit or delay or modulate processes with developer opt-in
   - architecture: see slides
   - carbon modelling: 4 components (i) geolocation via IP and GPS, (ii) power mix, and (iii) time of use, and (iv) forecasting for future tasks
   - carbon minimisation: (i) workload scheduling for tasks that do not need to be completed immediately, (ii) workload intensity throttling for tasks that do not have a critical completion time
   - Evaluation of the effect: energy consumption metrics and execution times, track baseline va. Substation performance, measuring over task-completion period
   - Current state: first prototype, no forecasting yet, only beginnings of carbon minimisation module
 * Q&A
   - C: You don't need to hide the carbon-intensity information in the framework. This can be used in for example a KDE widget and enable users to do things like decide when to run their dishwasher.
   - C: Looking at electricity maps, there's a Github repository with scripts to collect this information. Some of the forecast data comes from this. They filled the gaps with ML models. There may be options to gather the data on our own, but would require server-side infrastructure.
   - Q: Are we saving enough to make it worth the extra costs? A: One reason to avoid forecasting computing, which will offset the gains from Substation. Would need to look into throttling aspects of things.
     + C: I suspect delaying is where we would have the highest impact. This is relatively low overhead. Server side will be relatively constant. A widget though would create continuous costs.
     + C: Cloud-side came up in the CI discussion. Systematic rebuilds, cache updates, etc. One argument: it doesn't matter because we are running on renwable. But if we use clean energy, we make everyone else's a bit more dirty.
     + C: How you do forecasting may make a big difference. ML on historical data may look plausible, but may not be helpful. Perhaps just an average is enough. And for areas with a proper forecast, we can use that.
   - Q: How can you imagine KDE implementing this in our software? Good candidated for this? A: Discover would be a good starting point. KDenLive for video rendering would also be a good use case. Syncing, indexing, or other shiftable tasks would be a good use case.
     + Q: What is the time resolution of proprietary Electricity Maps? Free tier has an hourly resolution, with 50 calls an hour. We will probably want to use scripts found earlier (https://github.com/electricitymaps/electricitymaps-contrib) on our own infrastructure.
   - Q: To implement this, what kinds of changes would be needed on the developer side? A: Rough idea of what this can look like, but I cannot really say at the moment. The library provides tools for running a command or a command in a managed way (managed by substation). Developers would have to give parameters functions and they would be managed by substation.
     - C: This is something we need in KDE anyway to schedule long-term timers (e.g., scheduled alarms that tell the app to wake up when needed). We lack any kind of abstraction for that right now. E.g., I need a task run by a certain time is the same kind of issue.
   - Q: How can KDE Eco support this? A: Mostly been working on research side of things, implementation is not far along yet. Feedback at that stage would be very useful. Deadline for the Masters is in May, but will need to have a working prototype before then.
