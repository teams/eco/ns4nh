# Minutes for the Website Bloat BoF at Akademy 2024 on 2024-09-09

 * Start: 17:00 CEST; End: 17:50 CEST
 * About 10 participants
 * Note: These minutes were compiled both in person (special thanks to the volunteer!) as well as days after the BoF, so some details may be missing. Please feel free to add information to these minutes if you were in attendance.

## Discussion

 * Nowadays websites are super bloated and slow, which uses up a lot of energy. How do we fix it?
 * What happens on the web matters for efficiency. Browsers require a lot of energy.
 * Browsers/website loads are directly related to upcycling hardware and software: old HW may work fine for most tasks but struggle and become sluggish as soon as one opens a browser and goes online.
 * Custom fonts are really difficult to load and slow websites down. So do images.
 * You're usually on a website to get information, not enjoy a website experience (not necessarily true always).
 * Let's take the plank out of our own eye before we criticize other people's.
   - **KDE Eco Website load is 15mb**
   - Images are super big and don't need to be.
 * There are tools for image optimization (like Google Lightspeed, which includes accessibility stuff too). Lazy loading is good and easy to use.
 * You can use developer tools in Firefox to see how heavy a website is. Firefox network inspector lets you try different internet speeds, too.
 * In Hugo you can control the order of lazy loading operations by giving the image a specific size.
 * Why are we using so much JS? It is often unnecessary. HTML can do similar things for less cost and is better for accessibility.
 * **Trying to convince your job to take it seriously?**
   - Put the site into Lightspeed and explain how it improves SEO by loading faster. SEO is the magic word.
 * Some companies push things client side to save on their AWS costs. How can we avoid that or make it less typical.

## Improving KDE Websites

 * Things we can optimize on the KDE site: <https://invent.kde.org/teams/eco/opt-green/-/issues/37>
 * Lightspeed says the Eco website is 64 in mobile and 76 on broswer.
 * Biggest offenders across KDE sites are photos.
   - Fix with a commit hook limiting image size (if larger than 1 Mb, must resize and push a new MR.
   - Converting images to .webp format reduces size significantly.
 * One Simple Fix We Can Do Now: FIX THE PICS.
