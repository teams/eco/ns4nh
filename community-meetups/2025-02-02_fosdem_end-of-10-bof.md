
# End of 10 Campaign BoF

 * Announcement: [https://fosdem.org/2025/schedule/event/fosdem-2025-6779-coordinating-a-windows-10-to-linux-upcycling-campaign-across-free-software-communities-worldwide/](https://fosdem.org/2025/schedule/event/fosdem-2025-6779-coordinating-a-windows-10-to-linux-upcycling-campaign-across-free-software-communities-worldwide/)
 * Title: Coordinating a Windows 10-to-Linux upcycling campaign across Free Software communities worldwide
   - Room: H.3242, Track: BOF - Track B
   - Day: Sunday 14:00--15:00
 * Target audience: Anyone who has a win10 computer that can't be updated
   - Each community can target their own audience with their own priorities. KDE Eco for example is focusing on sustainability, but all reasons are good reasons. 
   - Onboard people with repair collectives and installation parties at local repair shops, hackerspaces, etc. Need to create a list of repair networks/cafes/etc who are interested in participating in onboarding new folks to linux so users can search for their options by locations. Currently KDE is in touch with repair cafes/collectives in Germany for this too.
   - Also have instructions for folks to do installations on their own.
 * Action Ideas:
   - Organizations/companies: if you can tell a computer is using Win10, direct people to endof10.org as a notification
   - Get in touch with journalists about there being other options for Win10's end of life (cf. Microsoft's browser antitrust case, which may have similarities)
   - Make a video about it: an old computer going out of support, and then comparing it with the KDE UI. "Hey it works so nice!"
   - Have a kit ready to go and events scheduled in advance. 
   - Easy migration website (goals)
   - Track of how many successful events have happened at repair centers (online meme campaigns, #EndOf10 hashtag)
   - For visitors to repair cafes: are there tax incentives related to this?
   - Get feedback/testimonials about user experiences for the website later (embed the hashtag to the website as a way to showcase this as well)
   - Can we get repair cafes to report any issues they run into (put this on the website: have a problem? report it here! Have it easy to click and fill for shops)
 * Things to overcome: 
   - The idea that Linux is too hard and too technical for the average user. Community support networks can help onboard new users. Ideally a virtuous cycle will occue: the more new users using Linux, the more they can help their friends/family, and they in turn can help others. The support network can only grow.
   - Available resources available for repair cafes. We would possibly need volunteers to either 1. train repair cafes to do Linux installs, or 2. get volunteers to join repair cafes.
   - Leverage change-management strategies to overcome people's fear of change
   - PEOPLE DON'T READ (make a video about it, that can also be shared with media)
   - SEO placement
   - Old HDDs may need to upgrade to SSD for better performance, but that costs money.
 * What is the scope? Currently, individual users, but if there's a path to onboard large orgs or corps to adopt that's cool too! 
   - eu-os.gitlab.io - is working on this on a corporate/gov't/organization/school/admin level to switch to something that isn't win11. Concerns are more security/user management based. 
 * To Dos:
   - Find and network with local repair cafes
   - SEO for the website 
   - Paid ads
   - Hashtag for social media? (positive campaign framing)
   - Have a messaging/media guide? Press release?
   - How to outreach broadly? (Journalists, orgs, etc - learn from people who've done this before!)
     + Get list of orgs 
     + Get list of reporters
     + Partner with gov'ts (local, state, etc) as well
     + Have some pamphlets that can be handed out at events, flea markets, etc
     + Make printable materials for folks to get involved, this can be part of a CTA
     + Local FOSS groups
     + bring materials to libraries
 * Where should we continue the conversation?
   - Invent (Gitlab) milestone: [https://invent.kde.org/teams/eco/opt-green/-/milestones/9#tab-issues](https://invent.kde.org/teams/eco/opt-green/-/milestones/9#tab-issues)
   - Matrix chat room: #win2linux:kde.org
