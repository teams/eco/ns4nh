# Minutes for the community meetup on 2024-07-10

* Start: 17:00 UTC; End: 18:00 UTC

* 3 participants [names removed to protect participant privacy]

* *Where*: BigBlueButton

* *Topic*: brainstorming engagement opportunities of the "Opt Green" project with the KDE community

* *Pad*: Ideas are collected at this pad, please add ideas of your own:

      https://collaborate.kde.org/s/cactBt4frrfTjbW

* *Details*: The goal of the new KDE Eco outreach project is to reduce e-waste by extending the operating lives of devices with sustainable KDE and Free Software. The primary target audience are "eco-consumers", those whose consumer behavior is driven by principles related to the environment. The project includes online and offline campaigns as well as upcycling workshops.

Now is an opportune time for the project's message. In 2025 end-of-support for Windows 10 is estimated to obsolete over 240 million devices. [1] Support for Intel Macs is "fading ever farther into Apple's rearview". [2] That's hundreds of million potential users who may want to keep using their devices, but may not know (yet!) that this is an option with Free Software. These are modern, powerful devices that KDE would run beautifully on.

But those are not the only devices the project is targeting. Working devices 15+ years old are also in our scope. How well does KDE Plasma run on these devices? How well do individual KDE apps? And how can we find out? (Perhaps we can hold an "oldest supported device" competition?) Is there interest from the community in trying to support hardware that old? What development work would that entail? These are just some of the questions coming up as we get the project off the ground. Others include:

 - Okular's eco-certification expires soon. Community interest in extending Blue Angel certification? Importance of certification for the project?
 - Integrating energy consumption measurements for more KDE software? The KEcoLab is all set up, but it needs more testing and more developers using it. [3] How can developers use this lab to make KDE more energy efficient?
 - Promoting transparency in energy efficiency and sustainability? Work on the Eco "About" dialogue has stalled. Anyone want to help us cross the finish line with that? [4]
 - How to give Plasma a little bit more personality? [5] How might branding help target our sustainability message to what KDE Plasma sees as potential users? [6]
 - State of immmutable OS devices (BananaOS, Kinoite [7], Kalpa [8])? Experiences? Are they ready for end users?

## Minutes

 * Introductions
 * Q(uestions) & A(nswers) & C(omments)
   - Q: Do we want to support hardware over 10 years old?
     + A: Age may be a good argument for marketing, but not for the technical side. What is important is the specs: CPU, graphic cards.
     + C: Embedded and mobile use cases are in the interest of KDE for hardware requirements, interest to run low spec hardware. But even there CPU/GPU and storage (flash memory drives) are what is relevant.
     + C: Windows 10 end of support, see: https://www.upgradetolinux.com/. Marketing idea: KDE Plasma can run on those Windows 10 computers.
     + C: https://www.golem.de/news/telemetrie-datenschutz-cloud-mein-wachsendes-unbehagen-mit-windows-2407-186820.html
     + C: Issue of DRM and media codecs: should be supported for everyday users.
     + C: Essentially only 2 browsers.
     + C: 32 bit support devices. Limited with web browsers, may fail on graphics performance. There may be use cases for this, but KDE is not the target audience.
   - Q: What are KDE minimal system requirements? Idea: Community competition to collect this data.
     + A: Avg specs of a 10 year old machine as a baseline.
     + A: Asking NGOs (e.g., ComputerSpende, ComputerTruhe, Repair Cafes) working on this to collect data about HW specs. Not only about what is doable, but info about real world use cases.
     + C: Web-based software is probably what most people are using.
     + C: Idea: Look for companies using older versions of software, there may be synergies there
     + C: Reluctant to recommend older versions to normal users because they will put there devices on the internet.
     + C: At some point for older devices it makes more sense to look at alternative Linux systems.
   - Q: Immutable OS topic
     + C: This might be bleeding edge development software. More of a distribution topic/challenge than a KDE topic.
     + C: May be very interesting to end users and more similar to what people expect from their phones.
   - Q: Plasma personality
     + C: More a design issue, outside programming.
     + C: Who is our audience? Everyone! Fancy and conventional? Enabled features by default? Give users easy ways to adjust settings for different computing environments.
     + C: It is more distributions that target this specifically. Distributions as the brand?
     + C: Even windows has different flavors for their products, for which distributions are filling the gap.
     + Need collaboration with the distros.

## Pre-meeting notes

 * Continued support for aging hardware? Defining minimum system requirements for KDE Plasma? KDE apps? 
   - KUserFeedback: mean age of hardware running software, oldest hardware, etc.
 * Community interest in extending Blue Angel certification? Significance for the project? Okular's eco-certification expires soon.
 * Integrating energy consumption measurement for more KDE software? The KEcoLab is all set up, but it needs more testing and more developers using it. https://invent.kde.org/teams/eco/remote-eco-lab
 * Promoting transparency in energy efficiency and sustainability? Work on the Eco "About" dialogue has stalled. Anyone want to help us cross the finish line with that? https://invent.kde.org/teams/eco/sustainable-software-goal/-/issues/2
   - Appstream implementation: need link by defining a custom tag in the application repository, see <https://invent.kde.org/graphics/okular/-/blob/master/shell/org.kde.okular.appdata.xml>
   - Generation script for apps.kde.org
 * How to give Plasma a little bit more personality and define the brand? How might this help target our message to potential KDE users? https://invent.kde.org/plasma/plasma-desktop/-/issues/132
 * State of immmutable OS devices (KDE BananaOS, Fedora Kinoite, openSUSE Kalpa)? Experiences? Ready for end users? 
   - https://fedoraproject.org/atomic-desktops/kinoite/
   - https://en.opensuse.org/Portal:Kalpa
 * KEcoLab progress 
   - R / LaTeX installation (see https://invent.kde.org/teams/eco/remote-eco-lab/-/issues/15 , https://invent.kde.org/teams/eco/remote-eco-lab/-/issues/17)
   - variable paths for scripts (see https://invent.kde.org/teams/eco/remote-eco-lab/-/issues/14)
 * Discussion: Merging feep / KEcoLab repos?
 * Status of packaging Selenium/KdeEcoTest in KDE Neon
 * Scripts for Okular v. Adobe Acrobat
 * Eco tab 
   - Appstream implementation: need link by defining a custom tag in the application repository, see <https://invent.kde.org/graphics/okular/-/blob/master/shell/org.kde.okular.appdata.xml>
   - Generation script for apps.kde.org
