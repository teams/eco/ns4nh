# SFSCon BoF

 * When: 8 November 2024, 14:40-15:20 CET
 * Where: SFSCon 2024

## Proposal

TITLE: Opt Green: Coordinating a Windows 10-to-Linux upcycling campaign across Free Software communities worldwide (14:40, 40 mins, 08 November 2024)

Windows 10 security updates end on 14 October 2025 KDE’s 29th birthday and also, ironically, International E-Waste Day (you cannot make these things up!). Hundreds of millions of functioning devices will become e-waste. This means manufacturing and transporting new ones, which is perhaps the biggest waste of all: hardware production alone can account for over 75% of a device’s CO2 emissions over its lifespan.

Free Software is a solution, and if we work together Windows 10 could truly be the last version of Windows users ever use! Let’s take this opportunity to coordinate a global, unified Free Software campaign over the next year to raise awareness about the environmental harm of software-driven hardware obsolescence, at the same time upgrading users from Windows 10 to GNU/Linux directly to keep those devices in use and out of the landfill. Let’s think big and act boldly! What should this campaign look like? Who is the target audience and how can we best reach them? What obstacles for new users exist and how can our communities best address them? Join the BoF to co-ordinate, together.

## Planned In-Person Follow-up BoFs

 * December: 38C3 in Hamburg
 * February: FOSDEM in Brussels

## Open Questions

 * What is the target audience of the campaign? What are their incentives to change?
 * Which Free Software communities want to participate?
 * What message can we all agree on?
 * What channels of communication should we use? Mailing list? Discuss (e.g., discuss.kde.org)? Matrix? Online meetups?

## Notes

 * Example unified campaign: The Day We Fight Back <https://en.wikipedia.org/wiki/The_Day_We_Fight_Back>
   - one-day global protest against mass surveillance
   - 11 February 2014, more than 6,000 participating websites
   - webpage banner: "Dear Internet, we're sick of complaining about the NSA. We want new laws that curtail online surveillance. Today we fight back."

## Protocol

 * Target audience?
   - eco-consumers? one issue is that a lot of those people are hipsters who use macs/surfaces. venn diagram between them and windows users not very large potentially?
   - older, e.g. 65+ users in retirement. they seem generally open to keeping their hardware in use for longer
   - or very young ones, who don't have jobs yet
     + they love games, steam as an example?
     + BUND youth group
     + idea: we can target parents, who can give hw to their kids
     + another idea: we target kids, who can help their parents and grandparents
   - high school to early college
     + save money by not buying a new laptop
     + starting where they are (e.g., they already know chromeos)
   - Users/consumers of other "ethical" branded products?
     + Fairphone, Ecosia, Fair trade stores
 * Other
   - People think it's normal that devices only last 5 years?
   - EOL moment as a moment of friction for many users
     + catch them at this moment and present an "easier" alternative vs. buying a new computer etc.
   - Providing Live USB sticks
   - Example marketing campaigns
     + Make your computer fast: https://getxtra-pc.io/offer-01/
     + Topio fair apps

