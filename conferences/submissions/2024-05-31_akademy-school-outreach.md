# Getting Them Early: Teaching Pupils About The Environmental Benefits Of FOSS

KDE Eco has a new project funded by the Umweltbundesamt (UBA) called "Sustainable Software For Sustainable Hardware". The goal of this project is to reduce e-waste by promoting the adoption of KDE / Free & Open Source Software (FOSS) and raising awareness about the critical role software plays in the long-term, efficient use of hardware.

In this lightning talk, I will discuss the work I am doing introducing KDE/FOSS to pupils, with a focus on its environmental benefits. I will share ideas on how to get schools involved in teaching pupils about reusing old hardware with FOSS. Finally, I will present some of the projects that we have already implemented in schools in Germany.

# Bio
Nicole Teale started at KDE in April 2024. She contributes to the KDE Eco project Nachhaltige Software Für Nachhaltige Hardware ('Sustainable Software For Sustainable Hardware').
