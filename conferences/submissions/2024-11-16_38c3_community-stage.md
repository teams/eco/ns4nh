# Opt Green: Coordinating a Windows 10-to-Linux upcycling campaign across Free Software communities worldwide

Windows 10 security updates end on 14 October 2025, KDE's 29th birthday and also, ironically, International E-Waste Day [1] (you cannot make these things up!). Hundreds of millions of functioning devices [2] will become e-waste. This means manufacturing and transporting new ones, which is perhaps the biggest waste of all: hardware production alone can account for over 75% of a device's CO2 emissions over its lifespan.

Free Software is a solution, today, and if we work together Windows 10 could truly be the last version of Windows users ever use! In this talk I will present the issue of e-waste and the importance of right-to-repair software, and invite the audience to participate in coordinating a global, unified Free Software campaign over the next year to raise awareness about the environmental harm of software-driven hardware obsolescence, while promoting upgrading users from Windows 10 to GNU/Linux directly. Extending hardware's operating life with Free Software is good for users, and better for the environment. Let's think big and act boldly as a unified community! 

[0] https://arstechnica.com/gadgets/2024/10/lots-of-pcs-are-poised-to-fall-off-the-windows-10-update-cliff-one-year-from-today/
[1] https://weee-forum.org/iewd-about/
[2] https://www.canalys.com/insights/end-of-windows-10-support-could-turn-240-million-pcs-into-e-waste

## Description

This is a talk about digital sustainability and the role software plays in hardware longevity.

## Speaker Bio

Joseph P. De Veaugh-Geiss (he/him) is community manager for the KDE Eco initiative, which aims to strengthen ecological sustainability as part of the development and adoption of Free Software.

