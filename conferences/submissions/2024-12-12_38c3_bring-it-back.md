# 38C3 Workshop: Bring it back to life! Kids Edition

## Beschreibung

### Deutsch

Hole ihn zurück ins Leben! Kinder-Edition

Schenk deinen Kindern zu den Feiertagen ein „neues“ altes Gerät für Linux-Spiele, sei es für Bildungs- oder Freizeitzwecke! Erwecke deinen alten Laptop zu neuem Leben und reduziere giftigen Elektroschrott mit Free & Open Source Software in diesem Drop-In Workshop mit KDE Eco (eco.kde.org). Das nachhaltigste Gerät ist das, das du bereits besitzt! Anfängerfreundlich, technische Kenntnisse sind nicht erforderlich. Wichtig: alte Laptops werden nicht zur Verfügung gestellt, bring einen mit.

#### Zusammenfassung

Schenk deinen Kindern in diesem Workshop mit KDE Eco ein „neues“ altes Linux-Spielgerät für die Feiertage.

### English

Bring it back to life! Kids Edition

Give your kids a “new” old device for Linux gaming this holiday season, whether for educational or recreational purposes! Bring your old laptop back to life and reduce toxic e-waste with KDE/Free & Open Source software in this drop-in workshop with KDE Eco (eco.kde.org). The most sustainable device is the one you already own! Beginner friendly, no technical knowledge required. Important: old laptops not provided, bring one with you.

#### Short Description

Give your kids a “new” old Linux gaming device for the holidays in this drop-in workshop with KDE Eco.

## Was benötigst du, um deinen Workshop durchzuführen?

Wir brauchen Steckdosenleisten, Tische, Stühle, und eventuell Netzwerk-Switch-Anschlüsse und Schreibtischlampen.

ca. 20 Personen.

## Aus welcher Community kommst du? Warst du Teilnehmer*in oder Mentor*in? Wo?

Solche Upcycling-Workshops sind Teil unseres neuen Projekts „Opt Green“, das wir 2024 mehrfach durchgeführt haben.
