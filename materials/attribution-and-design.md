# Attribution

## Directory `images/`

* The pie charts <apple-carbon-footprint-illustration-*> are published under a [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html) license and are to be attributed to "KDE". Design was by Anita Sengupta.
* The illuatration <e-waste-mountain-illustration.png> is published under a [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html) license and is to be attributed to "KDE". Design was by Anita Sengupta.
* The logos <KDE-eco-logo-K-domain.svg> and <KDE-eco-logo-K.svg> are published under a [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html) license and are to be attributed to "KDE". Design was by Lana Lutz.
* The image <okular-BE-logo.png> is published under a [CC-BY-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html) license and is to be attributed to "KDE". Design was by Paul Brown.
* The image <think-global-compute-local-DE.png> is published under a [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html) license and is to be attributed to "KDE". Modifications to the original were made by Nicole Teale.
* The image <think-global-compute-local-EN.png> is published under a [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html) license and is to be attributed to "KDE". Design was by Karanjot Singh.

## Directory `leaflets/`

* The leaflets <kde-eco-umweltfestival-flyer-*> are published under a [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html) license and are to be attributed to "KDE". Design was by Anita Sengupta.

## Directory `stickers/`

* Stickers <kde-eco-sticker.pdf> are published under a [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html) license and are to be attributed to "KDE". KDE Eco logo design was by Lana Lutz.
