# Opt Green

## Overview

"Opt Green: Nachhaltige Software Für Nachhaltige Hardware" (NS4NH) ("Sustainable Software For Sustainable Hardware") is a new KDE Eco project funded by the Umwelt Bundesamt. The goal of the project is to promote sustainable software and reducing e-waste by upcycling hardware with Free Software. The project will be funded from 1 April 2024 to 31 March 2026.

Through online and offline campaigns as well as installation workshops, KDE Eco will demonstrate at fair-trade, organic, and artisinal markets the power of Free Software to drive down energy consumption and keep devices in use for years beyond official vendor support. The offline campaigns will begin in Germany and around Europe and will later be taken to global communities with the support of KDE volunteers worldwide.

One target audience for the project are consumers whose behavior is driven by principles related to the environment, and not necesssarily convenience or cost: "eco-consumers".

## Communication

<a id="discuss"></a>

### Discussions

The Matrix room and discussion mailing list are good places for questions, community support, and general discussions around the topic.

The Matrix room is found at [#kde-eco:kde.org](https://webchat.kde.org/#/room/#kde-eco:kde.org). Here you can get involved or have a chat about this and other KDE Eco projects.

The discussion mailing list is [kde-eco-discuss@kde.org](https://mail.kde.org/cgi-bin/mailman/listinfo/kde-eco-discuss). Feel free to join there as well.

See the website for additional ways to get involved: https://eco.kde.org/get-involved/

### Announcements

To get announcements about KDE Eco events (e.g., workshops, stands at events), please subscribe to the announcement mailing list [kde-eco-announce@kde.org](https://mail.kde.org/cgi-bin/mailman/listinfo/kde-eco-announce). This will only be used for announcements. For discussion, see [above](#discuss).

## Contributions

This is a community project. Contributions are very welcome. Don't hesitate to [open issues](https://invent.kde.org/teams/eco/opt-green/-/issues/new) or [merge requests](https://invent.kde.org/teams/eco/opt-green/-/merge_requests/new).

This repository is maintained by Joseph P. De Veaugh-Geiss <joseph@kde.org>. If you have any questions or comments be in touch.

## Licenses

The content of this repository is licensed under the Creative Commons Attribution-Sharealike 4.0 International ([CC-BY-SA 4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html)), unless otherwise noted. See the LICENSES directory for the full texts of all relevant licenses. See the `attribution-and-design.md` documents for additional details.

## Funding Notice

This project was funded by the Federal Environment Agency and the Federal Ministry for the Environment, Nature Conservation, Nuclear Safety and Consumer Protection. The funds are made available by resolution of the German Bundestag.

<img src="uba/images/bmuv-DE.jpg" alt="BMU logo" width="300px"/>

<img src="uba/images/uba.jpg" alt="UBA logo" width="250px"/>

The publisher is responsible for the content of this publication.
